import setuptools
from pkg_resources import parse_requirements
from setuptools import setup

module_name = 'botsmanager'


def load_requirements(filename: str) -> list:
    requirements = []
    with open(filename, "r", encoding="utf-8") as f:
        for req in parse_requirements(f.read()):
            extras = "[{}]".format(','.join(req.extras)) if req.extras else ""
            requirements.append(
                '{}{}{}'.format(req.name, extras, req.specifier)
            )
    return requirements


console_scripts = [
    '{0}-run = {0}.api.__main__:main'.format(module_name),
    '{0}-db = {0}.api.db.__main__:main'.format(module_name),
    '{0}-sample-data = {0}.api.db.sample_data:main'.format(module_name),
]

setup(
    name=module_name,
    packages=setuptools.find_packages(where="src"),
    package_dir={'': 'src'},
    version="0.9.0",
    platforms='all',
    classifiers=[
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: CPython'
    ],
    python_requires='>=3.8',
    install_requires=load_requirements('requirements.txt'),
    extras_require={'dev': load_requirements('requirements.dev.txt')},
    entry_points={
        "console_scripts": console_scripts
    },
    include_package_data=True,
)
