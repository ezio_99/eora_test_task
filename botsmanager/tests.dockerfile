FROM snakepacker/python:all as builder

ENV PATH="/venv/bin/:${PATH}"

RUN python3.8 -m venv /venv
RUN /venv/bin/pip3 install -U pip

RUN /venv/bin/pip3 install wheel

COPY . /app

RUN ls -lah /app

WORKDIR /app

RUN /venv/bin/pip3 install ".[dev]"

FROM snakepacker/python:3.8 as api

ENV PATH="/venv/bin/:${PATH}"

COPY --from=builder /venv /venv
COPY --from=builder /app /app

RUN ls -lah /venv/bin

RUN /venv/bin/pip3 freeze

RUN apt update
RUN apt install -yqq curl

WORKDIR /app/src/botsmanager/api/db

CMD ["/app/scripts/tests_start"]
