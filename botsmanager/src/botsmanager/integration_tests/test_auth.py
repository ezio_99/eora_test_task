"""Require running app"""
import os

import aiohttp
import pytest

from botsmanager.api.settings import get_config_from_env

config = get_config_from_env()
HOST = os.getenv("API_TEST_HOST_DOCKER")

URL = f"http://{HOST}:{config['port']}"
print(URL)

login_credentials = {
    'email': "user1@test.com",
    'password': "test1234"
}

bad_credentials = {
    "email": "unknown@test.com",
    "password": "0"
}


@pytest.mark.asyncio
async def test_login_with_right_credentials():
    async with aiohttp.ClientSession() as session:
        async with session.post(URL + "/login", json=login_credentials) as response:
            assert response.status == 200
            body = await response.json()
            assert "token" in body
            assert "expires" in body


@pytest.mark.asyncio
async def test_login_with_bad_credentials():
    async with aiohttp.ClientSession() as session:
        async with session.post(URL + "/login", json=bad_credentials) as response:
            assert response.status == 400
