import sys

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor


def get_dispatcher(token: str):
    bot = Bot(token=token)
    dp = Dispatcher(bot)

    @dp.message_handler(commands=['start'])
    async def process_start_command(message: types.Message):
        await message.reply("Привет!\nНапиши мне что-нибудь!")

    @dp.message_handler(commands=['help'])
    async def process_help_command(message: types.Message):
        await message.reply("Напиши мне что-нибудь, и я отправлю этот текст тебе в ответ!")

    @dp.message_handler()
    async def echo_message(msg: types.Message):
        await bot.send_message(msg.from_user.id, msg.text)

    return dp


if __name__ == '__main__':
    token = sys.argv[1]
    dp = get_dispatcher(token)
    executor.start_polling(dp)
