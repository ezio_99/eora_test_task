import argparse
import os
import pathlib
import sys
from functools import partial

from trafaret_config import commandline

from botsmanager.api.utils.trafaret_ import TRAFARET

BASE_DIR = pathlib.Path(__file__).parent
PROJECT_DIR = BASE_DIR.parent
PROJECT_ROOT = BASE_DIR.parent.parent.parent.parent
DEFAULT_CONFIG_PATH = BASE_DIR / 'config' / 'api.yaml'

DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"


class EnvironmentVariableNotFound(Exception):
    pass


CONFIG_FILE_ENV_VAR_NAME = "API_CONFIG"


def get_config_from_env(ENV_NAME: str = CONFIG_FILE_ENV_VAR_NAME):
    parser = argparse.ArgumentParser()
    commandline.standard_argparse_options(parser, default_config=None)

    # ignore unknown options
    options, unknown = parser.parse_known_args()

    options.config = os.getenv(ENV_NAME)
    if not options.config:
        raise EnvironmentVariableNotFound(f"{ENV_NAME} not found.")

    config = commandline.config_from_options(options, TRAFARET)
    config['postgres']['dsn'] = DSN.format(**config['postgres'])

    return config


def get_config(argv=None):
    parser = argparse.ArgumentParser()
    commandline.standard_argparse_options(parser, default_config=DEFAULT_CONFIG_PATH)

    # ignore unknown options
    options, unknown = parser.parse_known_args(argv)
    print(options)

    config = commandline.config_from_options(options, TRAFARET)
    config['postgres']['dsn'] = DSN.format(**config['postgres'])
    return config


get_config_from_commandline = partial(get_config, sys.argv[1:])

get_config_for_tests = partial(get_config_from_env, "API_TEST_CONFIG")

