from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError

from botsmanager.api.db.schema import user_table
from botsmanager.api.settings import get_config_from_env
from botsmanager.api.utils.password import hash_password
from botsmanager.api.utils.pg import DSN


def sample_data(engine):
    conn = engine.connect()
    conn.execute(user_table.insert(), [
        {
            'id': 1,
            'email': "user1@test.com",
            'password': hash_password("test1234"),
            'name': "John",
            'surname': "Doe",
            'created': datetime.utcnow(),
        },
        {
            'id': 2,
            'email': "user2@test.com",
            'password': hash_password("test1234"),
            'name': "Anna",
            'surname': "Mitch",
            'created': datetime.utcnow(),
        },
    ])


def main():
    try:
        url = DSN.format(**get_config_from_env()['postgres'])
        engine = create_engine(url)
        sample_data(engine)
        print("Sample data added")
    except IntegrityError:
        print("Sample data already in db")


if __name__ == "__main__":
    main()
