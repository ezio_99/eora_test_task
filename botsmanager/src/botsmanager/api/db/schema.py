from datetime import datetime

from sqlalchemy import MetaData, Table, Integer, Column, String, ForeignKey, DateTime, UniqueConstraint

# https://docs.sqlalchemy.org/en/13/core/constraints.html#configuring-constraint-naming-conventions
convention = {
    'all_column_names': lambda constraint, table: '_'.join([
        column.name for column in constraint.columns.values()
    ]),
    'ix': 'ix__%(table_name)s__%(all_column_names)s',
    'uq': 'uq__%(table_name)s__%(all_column_names)s',
    'ck': 'ck__%(table_name)s__%(constraint_name)s',
    'fk': 'fk__%(table_name)s__%(all_column_names)s__%(referred_table_name)s',
    'pk': 'pk__%(table_name)s'
}
metadata = MetaData(naming_convention=convention)

user_table = Table(
    'user', metadata,

    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('email', String(200), nullable=False),
    Column('password', String(250), nullable=False),
    Column('name', String(50), nullable=False),
    Column('surname', String(50), nullable=False),
    Column('created', DateTime, default=datetime.utcnow(), nullable=False),

    UniqueConstraint('email')
)

bot_table = Table(
    'bot', metadata,

    Column('token', String(100), primary_key=True),
    Column('user_id', ForeignKey('user.id'), nullable=False),
    Column('added', DateTime, default=datetime.utcnow(), nullable=False),
)
