import logging

from botsmanager.api.settings import get_config_from_env

config = get_config_from_env()

logging.basicConfig(level=config["minimal_log_level"])
logger = logging.getLogger(__name__)

__all__ = (logger,)
