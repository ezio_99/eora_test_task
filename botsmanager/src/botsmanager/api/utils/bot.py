import enum
import os
import re
import subprocess
import sys
from typing import Union

from botsmanager.api.settings import PROJECT_DIR


class BotStatus(enum.Enum):
    running = "running"
    stopped = "stopped"


def get_ps_aux_output(token: str) -> str:
    proc1 = subprocess.Popen(['ps', 'aux'], stdout=subprocess.PIPE)
    proc2 = subprocess.Popen(['grep', token], stdin=proc1.stdout,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    proc1.stdout.close()
    out, _ = proc2.communicate()
    out = out.decode(encoding="utf-8")
    return out


pid_regex = re.compile(r"^\w+\s+(?P<pid>\d+)\s+.*$")


def get_pid(token: str) -> Union[None, int]:
    line_capture_regex = re.compile(rf"^.*/python[\d.]*\s+.*\.py\s+{token}\s*$")
    rows = get_ps_aux_output(token).split("\n")
    pid = None
    for row in rows:
        if stripped := row.strip():
            if line_capture_regex.match(stripped):
                pid = pid_regex.match(stripped).groupdict()["pid"]
                break
    return pid


def get_python_path():
    return sys.executable


def get_bot_file():
    return PROJECT_DIR / "bots" / "base.py"


def get_bot_status(token: str) -> (BotStatus, int):
    pid = get_pid(token)
    if pid is None:
        return BotStatus.stopped, pid
    else:
        return BotStatus.running, pid


def run_bot(token: str) -> None:
    os.system(f"nohup {get_python_path()} {get_bot_file()} {token} &")


def stop_bot(pid: int) -> None:
    os.system(f"kill {pid}")
