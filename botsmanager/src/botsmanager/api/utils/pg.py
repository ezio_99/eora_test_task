from aiohttp.web_app import Application
from asyncpgsa import PG

from botsmanager.api.settings import DSN
from botsmanager.api.utils.logger import logger

CENSORED = "***"
MAX_QUERY_ARGS = 32767
MAX_INTEGER = 2147483647


async def setup_pg(app: Application) -> PG:
    conf = app['config']['postgres']
    censored_dsn = DSN.format(user=conf['user'], password=CENSORED, host=conf['port'],
                              port=conf['port'], database=conf['database'])
    logger.info('Connecting to database: %s', censored_dsn)

    app['pg'] = PG()
    await app['pg'].init(conf['dsn'], min_size=conf['minsize'], max_size=conf['maxsize'])
    await app['pg'].fetchval('SELECT 1')
    logger.info('Connected to database %s', censored_dsn)

    try:
        yield
    finally:
        logger.info('Disconnecting from database %s', censored_dsn)
        await app['pg'].pool.close()
        logger.info('Disconnected from database %s', censored_dsn)
