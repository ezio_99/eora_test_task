from aiohttp import web

from botsmanager.api.app import create_app
from botsmanager.api.settings import get_config_from_env


def run_app():
    app = create_app()
    config = get_config_from_env()
    web.run_app(app, host=config['host'], port=config['port'])


def main():
    run_app()


if __name__ == '__main__':
    main()
