from functools import partial

from aiohttp import web
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware

from botsmanager.api.middlewares.validation_error import handle_validation_error
from botsmanager.api.utils.logger import logger
from botsmanager.api.utils.pg import setup_pg
from botsmanager.api.settings import get_config_from_env
from botsmanager.api.views import VIEWS

MB = 1024 ** 2
MAX_REQUEST_SIZE = 5 * MB


async def create_app():
    app = web.Application(
        client_max_size=MAX_REQUEST_SIZE,
        middlewares=[validation_middleware]
    )
    app['config'] = get_config_from_env()

    app.cleanup_ctx.append(partial(setup_pg))

    for view in VIEWS:
        logger.info('Registering view %r as %r', view, view.url)
        app.router.add_route('*', view.url, view)

    setup_aiohttp_apispec(app=app, title='Bots Manager API',
                          swagger_path='/docs',
                          url='/static/docs/swagger.json',
                          static_path='/static/swagger',
                          error_callback=handle_validation_error)

    return app
