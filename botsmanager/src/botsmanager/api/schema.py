from marshmallow import Schema
from marshmallow.fields import Str, Email, List, Nested
from marshmallow.validate import Length


class EmptySchema(Schema):
    pass


class RegisterSchema(Schema):
    email = Email(required=True)
    password = Str(validate=Length(min=8), required=True)
    name = Str(validate=Length(max=50), required=True)
    surname = Str(validate=Length(max=50), required=True)


class LoginSchema(Schema):
    email = Email(required=True)
    password = Str(validate=Length(min=8), required=True)


class LoginResponseSchema(Schema):
    token = Str(required=True)
    expires = Str(required=True)


class TokenSchema(Schema):
    token = Str(required=True)


class MSGSchema(Schema):
    msg = Str(required=True)


class BotInfoSchema(Schema):
    token = Str(required=True)
    status = Str(required=True)
    added = Str(required=True)


class ManyBotsInfoSchema(Schema):
    bots = List(Nested(BotInfoSchema, required=True))
