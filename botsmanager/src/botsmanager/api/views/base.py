from datetime import datetime

import jwt
from aiohttp.web_exceptions import HTTPForbidden, HTTPBadRequest
from aiohttp.web_request import Request
from aiohttp.web_urldispatcher import View
from asyncpgsa import PG


class BaseView(View):
    url: str

    @property
    def pg(self) -> PG:
        return self.request.app['pg']


def check_token(request):
    header = request.headers.get("Authorization")

    if header is None:
        raise HTTPBadRequest(text="No Authorization header")

    if not header.startswith("Bearer "):
        raise HTTPBadRequest(text="Header value should start with \"Bearer \"")

    token = header[7:]

    try:
        payload = jwt.decode(token, request.app["config"]["secret_key"], algorithms=["HS512"])
    except (jwt.DecodeError, jwt.ExpiredSignatureError):
        raise HTTPBadRequest(text="Invalid token")

    if datetime.fromisoformat(payload["expires"]) < datetime.utcnow():
        raise HTTPForbidden(text="Token expired")

    request["user_id"] = payload["user_id"]


class ProtectedView(BaseView):
    def __init__(self, request: Request):
        check_token(request)
        super().__init__(request)

    @property
    def user_id(self):
        return self.request["user_id"]


class ProtectedJSONView(ProtectedView):
    def __init__(self, request: Request):
        if request.content_type != "application/json":
            raise HTTPBadRequest(text="Content-Type should be application/json")
        super().__init__(request)
