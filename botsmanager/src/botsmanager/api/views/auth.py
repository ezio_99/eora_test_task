from datetime import timedelta, datetime
from http import HTTPStatus

import jwt
from aiohttp.web_exceptions import HTTPBadRequest
from aiohttp.web_response import json_response
from aiohttp_apispec import docs, request_schema, response_schema
from sqlalchemy import select

from .base import BaseView
from ..db.schema import user_table
from ..schema import LoginResponseSchema, LoginSchema, MSGSchema, RegisterSchema
from ..utils.password import verify_password, hash_password


class RegisterView(BaseView):
    url = "/register"

    @docs(summary="Register user.", tags=["public"])
    @request_schema(RegisterSchema())
    @response_schema(MSGSchema(), code=HTTPStatus.CREATED.value)
    async def post(self):
        body = await self.request.json()
        email = body["email"]
        password = body["password"]
        name = body["name"]
        surname = body["surname"]

        async with self.pg.transaction():
            query = select([
                user_table.c.id,
                user_table.c.email,
                user_table.c.password
            ]).where(user_table.c.email == email)
            user = await self.pg.fetchrow(query)

            if user:
                raise HTTPBadRequest(text=f"User with email {email} already exists")

            query = user_table.insert().values(email=email,
                                               password=hash_password(password),
                                               name=name,
                                               surname=surname)
            await self.pg.execute(query)

            return json_response({"msg": "user created"}, status=201)


class LoginView(BaseView):
    url = "/login"

    @docs(summary="Return Authorization token.", tags=["public"])
    @request_schema(LoginSchema())
    @response_schema(LoginResponseSchema(), code=HTTPStatus.OK.value)
    async def post(self):
        body = await self.request.json()
        email = body["email"]
        password = body["password"]
        expires = datetime.utcnow() + timedelta(hours=1)

        query = select([
            user_table.c.id,
            user_table.c.email,
            user_table.c.password
        ]).where(user_table.c.email == email)
        user = await self.pg.fetchrow(query)
        if not user:
            raise HTTPBadRequest(text=f"No user with email {email}")

        if verify_password(password, user["password"]):
            token = jwt.encode(
                {'user_id': user["id"], 'expires': expires.isoformat()},
                self.request.app["config"]["secret_key"], algorithm="HS512")
            return json_response({
                "token": token,
                "expires": expires.isoformat()
            })
        else:
            raise HTTPBadRequest(text="Bad password")
