from datetime import datetime
from http import HTTPStatus

from aiohttp.web_exceptions import HTTPBadRequest, HTTPNotFound
from aiohttp.web_response import json_response
from aiohttp_apispec import docs, response_schema, request_schema
from sqlalchemy import select, func, insert

from .base import ProtectedJSONView, ProtectedView
from ..db.schema import bot_table
from ..schema import TokenSchema, MSGSchema, BotInfoSchema, EmptySchema, ManyBotsInfoSchema
from ..utils.bot import run_bot, get_bot_status, BotStatus, stop_bot
from ..utils.logger import logger


class BotView(ProtectedJSONView):
    url = "/bot"

    async def check_quantity(self):
        query = select([
            func.count(bot_table.c.user_id).label("count")
        ]).where(bot_table.c.user_id == self.user_id)
        count = await self.pg.fetchval(query)
        return count < 5

    @docs(summary="Add bot's token to user's account and run bot.", tags=["protected"])
    @request_schema(TokenSchema())
    @response_schema(MSGSchema(), code=HTTPStatus.OK.value)
    async def post(self):
        async with self.pg.transaction():
            body = await self.request.json()
            token = body["token"]

            bot_added = False
            query = select([1]).where(bot_table.c.token == token)
            result = await self.pg.fetchval(query)
            if result:
                bot_added = True

            if not bot_added:
                if await self.check_quantity():
                    query = insert(bot_table).values(token=token, user_id=self.user_id, added=datetime.now())
                    await self.pg.execute(query)
                    run_bot(token)
                    return json_response({"msg": "bot added and started"})
                else:
                    raise HTTPBadRequest(text="no more bots allowed")

            else:  # bot added -> check status -> start if needed
                status, _ = get_bot_status(token)
                if status == BotStatus.stopped:
                    run_bot(token)
                    return json_response({"msg": "bot started"})
                else:
                    return json_response({"msg": "bot is already running"})

    @docs(summary="Get info about bot.", tags=["protected"])
    @request_schema(TokenSchema())
    @response_schema(BotInfoSchema(), code=HTTPStatus.OK.value)
    async def get(self):
        body = await self.request.json()
        token = body["token"]

        query = select([bot_table.c.token, bot_table.c.added]).where(bot_table.c.token == token)
        row = await self.pg.fetchrow(query)
        if not row["token"]:
            raise HTTPNotFound(text="No bot with provided token")

        return json_response({"token": token,
                              "status": get_bot_status(token)[0].value,
                              "added": row["added"].isoformat()})

    @docs(summary="Stop and delete bot.", tags=["protected"])
    @request_schema(TokenSchema())
    @response_schema(MSGSchema(), code=HTTPStatus.OK.value)
    async def delete(self):
        body = await self.request.json()
        token = body["token"]
        status, pid = get_bot_status(token)
        if status == BotStatus.running:
            stop_bot(pid)
        query = bot_table.delete().where(bot_table.c.token == token)
        await self.pg.execute(query)
        return json_response({"msg": "bot stopped and deleted"})


class AllBotsView(ProtectedView):
    url = "/bot/all"

    @docs(summary="Get info about all user's bots.", tags=["protected"])
    @response_schema(ManyBotsInfoSchema(), code=HTTPStatus.OK.value)
    async def get(self):
        query = select([bot_table.c.token, bot_table.c.added]).where(bot_table.c.user_id == self.user_id)
        rows = await self.pg.fetch(query)
        result = []
        for r in rows:
            result.append({
                "token": r["token"],
                "status": get_bot_status(r["token"])[0].value,
                "added": r["added"].isoformat(),
            })

        return json_response({"bots": result})


class StopBotView(ProtectedJSONView):
    url = "/bot/stop"

    @docs(summary="Stop bot.", tags=["protected"])
    @request_schema(TokenSchema())
    @response_schema(MSGSchema(), code=HTTPStatus.OK.value)
    async def post(self):
        body = await self.request.json()
        token = body["token"]

        status, pid = get_bot_status(token)
        if status == BotStatus.stopped:
            return json_response({"msg": "bot is already stopped"})
        else:
            logger.info("Stopping bot with pid %s", (pid,))
            stop_bot(pid)
            return json_response({"msg": "bot stopped"})
