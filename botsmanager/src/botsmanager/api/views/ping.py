from http import HTTPStatus

from aiohttp.web_response import json_response
from aiohttp_apispec import docs, response_schema

from .base import BaseView
from ..schema import MSGSchema


class PingView(BaseView):
    url = "/ping"

    @docs(summary="API's health indicator.", tags=["public"])
    @response_schema(MSGSchema(), code=HTTPStatus.OK.value)
    async def get(self):
        return json_response({"msg": "API is alive"})
