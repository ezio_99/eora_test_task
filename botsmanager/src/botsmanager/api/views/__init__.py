from .bot import BotView, AllBotsView, StopBotView
from .ping import PingView
from .auth import LoginView, RegisterView

VIEWS = (
    PingView,
    RegisterView,
    BotView,
    LoginView,
    AllBotsView,
    StopBotView,
)

__all__ = (VIEWS,)
