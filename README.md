# About project

Test task for EORA (for Summer internship 2021). Service for managing Telegram bots.

# Deploy

Before deploying put your secure data into `botsmanager/.env` and `botsmanager/scripts/env`, because values, presented
in these files ONLY for example.

Assumed that `pgdata` directory is not exists.

    bash -c "source botsmanager/scripts/env && botsmanager/scripts/init_db"

Launching containers.

    docker-compose -f docker-compose.prod.yaml --env-file botsmanager/.env up

# Configuring reverse proxy with SSL

Install necessary packets.

    apt install nginx certbot

Put content below to `/etc/letsencrypt/cli.ini`.

    authenticator = webroot
    webroot-path = /var/www/html
    post-hook = service nginx reload
    text = True

Comment line `include /etc/nginx/sites-enabled/*;` in `/etc/nginx/nginx.conf`.

Register in Let's Encrypt.

    certbot register --email <your_email>

Put content below to `/etc/nginx/conf.d/botsmanager.conf`.

    server {
         server_name <your_domain>; listen <your_domain>:443 ssl;

         ssl_certificate /etc/letsencrypt/live/<your_domain>/fullchain.pem;
         ssl_certificate_key /etc/letsencrypt/live/<your_domain>/privkey.pem;
         ssl_trusted_certificate /etc/letsencrypt/live/<your_domain>/chain.pem;

         ssl_stapling on;
         ssl_stapling_verify on;
         resolver 127.0.0.1 8.8.8.8;

         add_header Strict-Transport-Security "max-age=31536000";

         add_header Content-Security-Policy "img-src https: data:; upgrade-insecure-requests";

         location /.well-known {
            root /var/www/html;
         }
    
         location /api {
            proxy_pass http://127.0.0.1:80;
            rewrite ^/api/(.*) /$1 break;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP $remote_addr;
         }

        location /static {
            proxy_pass http://127.0.0.1:80;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Real-IP $remote_addr;
        }

        location / {
            return 301 https://www.eora-test-task.dns-cloud.net/api/ping;
        }
    }

Get certificates.

    certbot certonly --expand -d <your_domain>

For setup certificates auto-update change `certbot -q renew` in .`/etc/cron.d/certbot`
to `certbot -q renew --allow-subset-of-names`

# Running tests

    botsmanager/scripts/drop_test_db
    botsmanager/scripts/init_test_db
    docker-compose -f docker-compose.tests.yaml --env-file botsmanager/.env up

# Documentation

API documentation available in `/docs`.
